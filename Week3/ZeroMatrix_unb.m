
% Copyright 2015 The University of Texas at Austin
%
% For licensing information see
%                http://www.cs.utexas.edu/users/flame/license.html 
%                                                                                 
% Programmed by: Name of author
%                Email of author

function [ at_out ] = ZeroMatrix_unb( at )

  [ aLt, aRt ] = FLA_Part_1x2( at, ...
                                 0, 'FLA_LEFT' );

  while ( size( aLt, 2 ) < size( at, 2 ) )

    [ a0t, alpha1, a2t ]= FLA_Repart_1x2_to_1x3( aLt, aRt, ...
                                               1, 'FLA_RIGHT' );

    %------------------------------------------------------------%

	alpha1 = laff_zerov(alpha1);

    %------------------------------------------------------------%

    [ aLt, aRt ] = FLA_Cont_with_1x3_to_1x2( a0t, alpha1, a2t, ...
                                             'FLA_LEFT' );

  end

  at_out = [ aLt, aRt ];

return
