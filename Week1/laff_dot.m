function [ alpha ] = laff_dot( x, y)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[ r_x, c_x] = size(x);
[ r_y, c_y] = size(y);
out = 0;
for i=1:r_x
   for j=1:c_x
	  out = out + x(i, j) * y(i, j);
   end
end
alpha = out;
return
end

