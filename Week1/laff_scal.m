function [ x_out ] = laff_scal( alpha, x )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[ r_x, c_x] = size(x);

for i=1:r_x
   for j=1:c_x
	  x_out(i, j) = alpha * x(i, j);
   end
end

%x_out = x;
return;
end

