function [ alpha ] = laff_norm2( x )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[ r_x, c_x ] = size( x );

tempVar = 0;

if r_x ~= 1
	for i=1:r_x
	   tempVar =  tempVar + x(i) * x(i);
	end
else
	for j=1:c_x
		tempVar = tempVar + x(1, j) * x (1, j);
	end
end

alpha = sqrt(tempVar);

return 

end

