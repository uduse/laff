function [ x_out ] = laff_axpy( alpha, x, y)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[ r_x, c_x] = size(x);
[ r_y, c_y] = size(y);

for i=1:r_x
	for j=1:c_x
	x_out(i, j) = x(i, j) * alpha + y(i, j);
   end
end

return

end

